#pragma once
#include "ISoundController.h"
#include <map>

namespace FMOD {

	class System;
	class Channel;
	class Sound;
}

namespace SoundProgrLib {

	class SoundController : public ISoundController
	{
	private:

		static constexpr int NUM_CHANNELS = 10;
		static constexpr int MIN_PAN = -1;
		static constexpr int MAX_PAN = 1;
		static constexpr int MIN_VOLUME = 0;
		static constexpr int MAX_VOLUME = 1;

		FMOD::System* m_FMODSystem;
		FMOD::Channel** m_channels;
		FMOD::Sound** m_sounds;

	public:

		SoundController();

		virtual void Init() override;
		virtual void LoadStatic(std::string i_resource, int i_channel) override;
		virtual void LoadStream(std::string i_resource, int i_channel) override;
		virtual void Unload(int i_channel) override;
		virtual void Play(int i_channel) override;
		virtual void Pause(int i_channel) override;
		virtual void Stop(int i_channel) override;
		virtual void SetLoop(bool i_loop, int i_channel) override;
		virtual void SetPan(float i_pan, int i_channel) override;
		virtual float GetPan(int i_channel) override;
		virtual void SetVolume(float i_volume, int i_channel) override;
		virtual float GetVolume(int i_channel) override;
		virtual int GetNumChannels() override;
		virtual ChannelStatus GetChannelStatus(int i_channel) override;
		virtual bool HasSound(int i_channel) override;
		virtual std::string GetSoundName(int i_channel) override;

		virtual ~SoundController() override;

	};

}