#pragma once
#include <iostream>

namespace FMOD {

	class Sound;
}

namespace SoundProgrLib {

	enum ChannelStatus {
		play,
		pause,
		stop
	};

	class ISoundController {
	public:
		virtual void Init() = 0;
		virtual void LoadStatic(std::string i_resource, int i_channel) = 0;
		virtual void LoadStream(std::string i_resource, int i_channel) = 0;
		virtual void Unload(int i_channel) = 0;
		virtual void Play(int i_channel) = 0;
		virtual void Pause(int i_channel) = 0;
		virtual void Stop(int i_channel) = 0;
		virtual void SetLoop(bool i_loop, int i_channel) = 0;
		virtual void SetPan(float i_pan, int i_channel) = 0;
		virtual float GetPan(int i_channel) = 0;
		virtual void SetVolume(float i_volume, int i_channel) = 0;
		virtual float GetVolume(int i_channel) = 0;
		virtual int GetNumChannels() = 0;
		virtual ChannelStatus GetChannelStatus(int i_channel) = 0;
		virtual bool HasSound(int i_channel) = 0;
		virtual std::string GetSoundName(int i_channel) = 0;

		virtual ~ISoundController() {}
	};

}