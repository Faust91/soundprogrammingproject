#include "pch.h"
#include "framework.h"
#include "SoundController.h"
#include "fmod.hpp"
#include <string>

namespace SoundProgrLib {

	SoundController::SoundController() :
		m_FMODSystem(nullptr),
		m_channels(nullptr),
		m_sounds(nullptr)
	{

	}

	void SoundController::Init()
	{
		FMOD_RESULT result = FMOD::System_Create(&m_FMODSystem);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			throw std::runtime_error("Cannot create FMOD system.");
			return;
		}

		result = m_FMODSystem->init(NUM_CHANNELS, FMOD_INIT_NORMAL, 0);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			throw std::runtime_error("Cannot init channels.");
			return;
		}

		m_channels = new FMOD::Channel * [NUM_CHANNELS];
		m_sounds = new FMOD::Sound * [NUM_CHANNELS];
	}

	void SoundController::LoadStatic(std::string i_resource, int i_channel)
	{
		FMOD::Sound* sound;
		FMOD_RESULT result = m_FMODSystem->createSound(i_resource.c_str(), FMOD_CREATESAMPLE, nullptr, &sound);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			throw std::runtime_error("Cannot load static sound.");
			return;
		}
		m_sounds[i_channel] = sound;
	}

	void SoundController::LoadStream(std::string i_resource, int i_channel)
	{
		FMOD::Sound* sound;
		FMOD_RESULT result = m_FMODSystem->createSound(i_resource.c_str(), FMOD_CREATESTREAM, nullptr, &sound);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			throw std::runtime_error("Cannot load stream sound.");
			return;
		}
		m_sounds[i_channel] = sound;
	}

	void SoundController::Unload(int i_channel)
	{
		m_sounds[i_channel]->release();
		m_sounds[i_channel] = nullptr;
	}

	void SoundController::Play(int i_channel)
	{
		ChannelStatus status = GetChannelStatus(i_channel);
		if (status == ChannelStatus::pause) {
			FMOD_RESULT result = m_channels[i_channel]->setPaused(false);
			if (result != FMOD_RESULT::FMOD_OK)
			{
				throw std::runtime_error("Unable to resume.");
				return;
			}
		}
		else if (status == ChannelStatus::stop && m_sounds[i_channel] != nullptr) {
			if (HasSound(i_channel)) {
				FMOD_RESULT result = m_FMODSystem->playSound(m_sounds[i_channel], 0, false, &m_channels[i_channel]);
				if (result != FMOD_RESULT::FMOD_OK)
				{
					throw std::runtime_error("Unable to play.");
					return;
				}
			}
		}
	}

	void SoundController::Pause(int i_channel)
	{
		ChannelStatus status = GetChannelStatus(i_channel);
		if (status == ChannelStatus::play) {
			FMOD_RESULT result = m_channels[i_channel]->setPaused(true);
			if (result != FMOD_RESULT::FMOD_OK)
			{
				throw std::runtime_error("Unable to pause.");
				return;
			}
		}
	}

	void SoundController::Stop(int i_channel)
	{
		ChannelStatus status = GetChannelStatus(i_channel);
		if (status != ChannelStatus::stop) {
			FMOD_RESULT result = m_channels[i_channel]->stop();
			if (result != FMOD_RESULT::FMOD_OK)
			{
				throw std::runtime_error("Unable to stop.");
				return;
			}
		}
	}

	void SoundController::SetLoop(bool i_loop, int i_channel)
	{
		if (HasSound(i_channel)) {
			bool paying = GetChannelStatus(i_channel) != ChannelStatus::stop;
			if (paying) {
				Stop(i_channel);
			}
			FMOD_RESULT result = m_sounds[i_channel]->setMode(i_loop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF);
			if (result != FMOD_RESULT::FMOD_OK)
			{
				throw std::runtime_error("Cannot set loop.");
				return;
			}
		}
	}

	void SoundController::SetPan(float i_pan, int i_channel)
	{
		if (i_pan < MIN_PAN || i_pan > MAX_PAN) {
			throw std::runtime_error("Wrong pan. Pan must be between " + std::to_string(MIN_PAN) + " and " + std::to_string(MAX_PAN) + ".");
			return;
		}
		FMOD_RESULT result = m_channels[i_channel]->setPan(i_pan);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
		{
			throw std::runtime_error("Cannot set pan.");
			return;
		}
	}

	float SoundController::GetPan(int i_channel)
	{
		float mixMatrix[FMOD_MAX_CHANNEL_WIDTH][FMOD_MAX_CHANNEL_WIDTH] = {};
		int out, in;

		FMOD_RESULT result = m_channels[i_channel]->getMixMatrix(*mixMatrix, &out, &in);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
		{
			throw std::runtime_error("Cannot get mix matrix.");
			return 0.0f;
		}
		if (result == FMOD_RESULT::FMOD_ERR_INVALID_HANDLE) {
			return 0.0f;
		}
		float left = 1.0f - (2 * (mixMatrix[0][0] * mixMatrix[0][0]));
		float right = (2 * (mixMatrix[1][0] * mixMatrix[1][0]) - 1.0f);

		float pan = (left + right) * 0.5f;

		return pan < -1.0f ? -1.0f : pan > 1.0f ? 1.0f : pan;

	}

	void SoundController::SetVolume(float i_volume, int i_channel)
	{
		if (i_volume < MIN_VOLUME || i_volume > MAX_VOLUME) {
			throw std::runtime_error("Wrong volume. Volume must be between " + std::to_string(MIN_VOLUME) + " and " + std::to_string(MAX_VOLUME) + ".");
			return;
		}
		FMOD_RESULT result = m_channels[i_channel]->setVolume(i_volume);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
		{
			throw std::runtime_error("Cannot set volume.");
			return;
		}
	}

	float SoundController::GetVolume(int i_channel)
	{
		float volume;
		FMOD_RESULT result = m_channels[i_channel]->getVolume(&volume);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
		{
			throw std::runtime_error("Cannot get volume.");
			return 0.0f;
		}
		if (result == FMOD_RESULT::FMOD_ERR_INVALID_HANDLE) {
			return 0.0f;
		}
		return volume;
	}

	int SoundController::GetNumChannels()
	{
		return NUM_CHANNELS;
	}

	SoundProgrLib::ChannelStatus SoundController::GetChannelStatus(int i_channel)
	{
		bool playing = false;
		FMOD_RESULT result = m_channels[i_channel]->isPlaying(&playing);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE && result != FMOD_ERR_CHANNEL_STOLEN)
		{
			throw std::runtime_error("Cannot determine if playing.");
			return ChannelStatus::stop;
		}
		if (playing) {
			bool paused;
			result = m_channels[i_channel]->getPaused(&paused);
			if (result != FMOD_RESULT::FMOD_OK)
			{
				throw std::runtime_error("Cannot determine if paused.");
				return ChannelStatus::stop;
			}
			if (paused) {
				return ChannelStatus::pause;
			}
			return ChannelStatus::play;
		}
		return ChannelStatus::stop;
	}

	bool SoundController::HasSound(int i_channel)
	{
		FMOD_OPENSTATE openState;
		unsigned int percentBuffered;
		bool starving;
		bool diskbusy;
		FMOD_RESULT result = m_sounds[i_channel]->getOpenState(&openState, &percentBuffered, &starving, &diskbusy);
		if (result != FMOD_RESULT::FMOD_OK && result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
		{
			throw std::runtime_error("Unable to determine state.");
			return false;
		}
		if (result == FMOD_RESULT::FMOD_ERR_INVALID_HANDLE) {
			return false;
		}
		if (openState != FMOD_OPENSTATE_ERROR) {
			return true;
		}
		return false;
	}

	std::string SoundController::GetSoundName(int i_channel)
	{
		char name[512] = {};
		int namelen = 512;
		FMOD_RESULT result = m_sounds[i_channel]->getName(name, namelen);
		if (result != FMOD_RESULT::FMOD_OK)
		{
			throw std::runtime_error("Cannot retrieve sound name.");
			return "";
		}
		return name;
	}

	SoundController::~SoundController()
	{
		for (int i = 0; i < NUM_CHANNELS; i++) {
			if (m_channels[i] != nullptr) {
				m_channels[i]->stop();
			}
			if (m_sounds[i] != nullptr) {
				m_sounds[i]->release();
			}
		}

		if (m_FMODSystem != nullptr) {
			m_FMODSystem->release();
		}
	}

}