#pragma once
#include <string>
#include <vector>

namespace SoundProgrLib {
	class ISoundController;
}

class SoundClient
{
private:
	SoundProgrLib::ISoundController* m_soundController;
	int m_currentChannel;

public:
	SoundClient();
	std::vector<std::string> GetInfo();
	std::string GetStatus();
	std::string GetSoundName();
	int GetNumChannels();
	int GetChannel();
	void SetChannel(int i_channel);
	void LoadSound(std::string i_sound, bool i_stream);
	void UnloadSound();
	void SetLoop(bool i_loop);
	void Play(bool i_forAll);
	void Pause(bool i_forAll);
	void Stop(bool i_forAll);
	int GetPan();
	void SetPan(int i_pan, bool i_forAll);
	int GetVolume();
	void SetVolume(int i_volume, bool i_forAll);

	~SoundClient();
};

