// SoundProgrClient.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "SoundClient.h"
#include <iostream>

int Menu(SoundClient&);
void ClearScreen();
void ManageError(std::string i_errorText);
std::string TranslateSound(int i_num);

void Goodbye();
void SelectChannel(SoundClient& i_client);
void LoadSound(SoundClient& i_client, bool i_stream);
void UnloadSound(SoundClient& i_client);
void SetLoop(SoundClient& i_client, bool i_loop);
void Play(SoundClient& i_client, bool i_forAll);
void Pause(SoundClient& i_client, bool i_forAll);
void Stop(SoundClient& i_client, bool i_forAll);
void SetPan(SoundClient& i_client, bool i_forAll);
void SetVolume(SoundClient& i_client, bool i_forAll);

int main()
{
	SoundClient soundClient = SoundClient();
	int choice = 0;
	do {
		choice = Menu(soundClient);
		switch (choice) {
		case 0:
			Goodbye();
			break;
		case 1:
			SelectChannel(soundClient);
			break;
		case 2:
			LoadSound(soundClient, false);
			break;
		case 20:
			LoadSound(soundClient, true);
			break;
		case 200:
			UnloadSound(soundClient);
			break;
		case 3:
			SetLoop(soundClient, true);
			break;
		case 30:
			SetLoop(soundClient, false);
			break;
		case 4:
			Play(soundClient, false);
			break;
		case 40:
			Play(soundClient, true);
			break;
		case 5:
			Pause(soundClient, false);
			break;
		case 50:
			Pause(soundClient, true);
			break;
		case 6:
			Stop(soundClient, false);
			break;
		case 60:
			Stop(soundClient, true);
			break;
		case 7:
			SetPan(soundClient, false);
			break;
		case 70:
			SetPan(soundClient, true);
			break;
		case 8:
			SetVolume(soundClient, false);
			break;
		case 80:
			SetVolume(soundClient, true);
			break;
		case 9:
			Menu(soundClient);
			break;
		default:
			ManageError("Command unknown.");
			break;
		}
	} while (choice != 0);
}

int Menu(SoundClient& client) {

	ClearScreen();

	for (std::string info : client.GetInfo()) {
		std::cout << info << std::endl;
	}
	std::cout << std::endl;
	std::cout << "[1] Select channel" << std::endl;
	std::cout << "[2] Load static sound / [20] Load stream sound / [200] Unload" << std::endl;
	std::cout << "[3] Enable loop / [30] Disable loop" << std::endl;
	std::cout << "[4] Play / [40] Play all" << std::endl;
	std::cout << "[5] Pause / [50] Pause all" << std::endl;
	std::cout << "[6] Stop / [60] Stop all" << std::endl;
	std::cout << "[7] Set pan / [70] Set pan for all" << std::endl;
	std::cout << "[8] Set volume / [80] Set volume for all" << std::endl;
	std::cout << "[9] Refresh" << std::endl;
	std::cout << "[0] Exit" << std::endl;

	int choice;
	std::cin >> choice;

	ClearScreen();

	return choice;
}

void ClearScreen() {
#if defined _WIN32
	system("cls");
#elif defined (LINUX) || defined(gnu_linux) || defined(linux)
	system("clear");
#elif defined (APPLE)
	system("clear");
#endif
}

void ManageError(std::string i_errorText) {
	ClearScreen();
	std::cout << i_errorText << std::endl;
	std::cout << "Press ENTER to go back." << std::endl;
	std::cin.ignore();
	std::getchar();
}

std::string TranslateSound(int i_num) {
	std::string sound;
	switch (i_num) {
	case 1:
		sound = "music.wav";
		break;
	case 2:
		sound = "bass.wav";
		break;
	case 3:
		sound = "voice.wav";
		break;
	case 4:
		sound = "crash.wav";
		break;
	default:
		sound = "";
		break;
	}
	return sound;
}

void Goodbye() {
	std::cout << std::endl;
	std::cout << "Goodbye" << std::endl;
	std::cout << std::endl;
}

void SelectChannel(SoundClient& i_client) {
	std::cout << "Which channel?";
	std::cout << " ";
	std::cout << "[" << "0" << " - " << std::to_string(i_client.GetNumChannels() - 1) << "]";
	std::cout << std::endl;

	std::cin.ignore();
	int intVal;
	std::cin >> intVal;

	try {
		i_client.SetChannel(intVal);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void LoadSound(SoundClient& i_client, bool i_stream) {

	std::cout << "Which sound?";
	std::cout << std::endl;
	std::cout << "[1] Music" << std::endl;
	std::cout << "[2] Bass" << std::endl;
	std::cout << "[3] Voice" << std::endl;
	std::cout << "[4] Crash" << std::endl;

	std::cin.ignore();
	int intVal;
	std::cin >> intVal;

	try {
		i_client.LoadSound(TranslateSound(intVal), i_stream);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void UnloadSound(SoundClient& i_client) {
	try {
		i_client.UnloadSound();
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void SetLoop(SoundClient& i_client, bool i_loop) {
	try {
		i_client.SetLoop(i_loop);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void Play(SoundClient& i_client, bool i_forAll) {
	try {
		i_client.Play(i_forAll);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void Pause(SoundClient& i_client, bool i_forAll) {
	try {
		i_client.Pause(i_forAll);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void Stop(SoundClient& i_client, bool i_forAll) {
	try {
		i_client.Stop(i_forAll);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void SetPan(SoundClient& i_client, bool i_forAll) {
	std::cout << "Set pan";
	std::cout << " ";
	std::cout << "[" << "-10" << " - " << "10" << "]";
	std::cout << std::endl;

	std::cin.ignore();
	int intVal;
	std::cin >> intVal;

	try {
		i_client.SetPan(intVal, i_forAll);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

void SetVolume(SoundClient& i_client, bool i_forAll) {
	std::cout << "Set volume";
	std::cout << " ";
	std::cout << "[" << "0" << " - " << "10" << "]";
	std::cout << std::endl;

	std::cin.ignore();
	int intVal;
	std::cin >> intVal;

	try {
		i_client.SetVolume(intVal, i_forAll);
	}
	catch (std::runtime_error e) {
		ManageError(e.what());
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
