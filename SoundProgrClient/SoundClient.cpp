#include "SoundClient.h"
#include "SoundController.h"

SoundClient::SoundClient() : m_soundController(new SoundProgrLib::SoundController()), m_currentChannel(0)
{
	m_soundController->Init();
}

std::vector<std::string> SoundClient::GetInfo()
{
	std::vector<std::string>  vector;
	std::string info;

	info = "";
	info = info + "          ";
	for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
		info = info + " " + (i == m_currentChannel ? "v" : " ") + " ";
	}
	vector.push_back(info);

	info = "";
	info = info + "Channel:  ";
	for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
		info = info + "[" + std::to_string(i) + "]";
	}
	vector.push_back(info);

	info = "";
	info = info + "Status:   ";
	for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
		SoundProgrLib::ChannelStatus status = m_soundController->GetChannelStatus(i);
		std::string statusString = " ";
		switch (status) {
		case SoundProgrLib::play:
			statusString = ">";
			break;
		case SoundProgrLib::pause:
			statusString = "|";
			break;
		case SoundProgrLib::stop:
			statusString = "X";
			break;
		}
		info = info + "[" + statusString + "]";
	}
	vector.push_back(info);

	info = "";
	info = info + "Has sound:";
	for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
		bool hasSound = m_soundController->HasSound(i);
		std::string loadString = (hasSound ? "Y" : " ");
		info = info + "[" + loadString + "]";
	}
	vector.push_back(info);

	info = "";
	info = info + "          ";
	for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
		info = info + " " + (i == m_currentChannel ? "^" : " ") + " ";
	}
	vector.push_back(info);

	info = "";
	vector.push_back(info);

	info = "";
	info = info + "Current: " + std::to_string(m_currentChannel);
	vector.push_back(info);

	info = "";
	info = info + "Status:  " + GetStatus();
	vector.push_back(info);

	info = "";
	info = info + "Sound:   " + GetSoundName();
	vector.push_back(info);

	//info = "";
	//info = info + "Pan:     " + std::to_string(GetPan());
	//vector.push_back(info);

	info = "";
	info = info + "Volume:  " + std::to_string(GetVolume());
	vector.push_back(info);

	return vector;
}

std::string SoundClient::GetStatus()
{
	SoundProgrLib::ChannelStatus status = m_soundController->GetChannelStatus(m_currentChannel);
	std::string statusString;
	switch (status) {
	case SoundProgrLib::play:
		statusString = "PLAY";
		break;
	case SoundProgrLib::pause:
		statusString = "PAUSE";
		break;
	case SoundProgrLib::stop:
		statusString = "STOP";
		break;
	default:
		statusString = "UNKNOWN";
		break;
	}
	return statusString;
}

std::string SoundClient::GetSoundName()
{
	if (m_soundController->HasSound(m_currentChannel)) {
		return m_soundController->GetSoundName(m_currentChannel);
	}
	else {
		return "";
	}
}

int SoundClient::GetNumChannels()
{
	return m_soundController->GetNumChannels();
}

int SoundClient::GetChannel()
{
	return m_currentChannel;
}

void SoundClient::SetChannel(int i_channel)
{
	if (i_channel < 0 || i_channel >= m_soundController->GetNumChannels()) {
		throw std::runtime_error("Wrong value. Channel number must be between 0 and " + std::to_string(m_soundController->GetNumChannels() - 1) + ".");
	}
	m_currentChannel = i_channel;
}

void SoundClient::LoadSound(std::string i_sound, bool i_stream)
{
	if (i_stream) {
		m_soundController->LoadStream(i_sound, m_currentChannel);
	}
	else {
		m_soundController->LoadStatic(i_sound, m_currentChannel);
	}
}

void SoundClient::UnloadSound()
{
	m_soundController->Unload(m_currentChannel);
}

void SoundClient::SetLoop(bool i_loop)
{
	m_soundController->SetLoop(i_loop, m_currentChannel);
}

void SoundClient::Play(bool i_forAll)
{
	if (i_forAll) {
		for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
			m_soundController->Play(i);
		}
	}
	else {
		m_soundController->Play(m_currentChannel);
	}
}

void SoundClient::Pause(bool i_forAll)
{
	if (i_forAll) {
		for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
			m_soundController->Pause(i);
		}
	}
	else {
		m_soundController->Pause(m_currentChannel);
	}
}

void SoundClient::Stop(bool i_forAll)
{
	if (i_forAll) {
		for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
			m_soundController->Stop(i);
		}
	}
	else {
		m_soundController->Stop(m_currentChannel);
	}
}

int SoundClient::GetPan()
{
	float pan = m_soundController->GetPan(m_currentChannel);
	int panInt = (int)(pan * 10.0f);
	return panInt;
}

void SoundClient::SetPan(int i_pan, bool i_forAll)
{
	if (i_pan < -10 || i_pan > 10) {
		throw std::runtime_error("Wrong value. Pan must be between -10 and 10.");
	}
	float pan = (float)i_pan / 10.0f;
	if (i_forAll) {
		for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
			m_soundController->SetPan(pan, i);
		}
	}
	else {
		m_soundController->SetPan(pan, m_currentChannel);
	}
}

int SoundClient::GetVolume()
{
	float volume = m_soundController->GetVolume(m_currentChannel);
	int volumeInt = (int)(volume * 10.0f);
	return volumeInt;
}

void SoundClient::SetVolume(int i_volume, bool i_forAll)
{
	if (i_volume < 0 || i_volume > 10) {
		throw std::runtime_error("Wrong value. Volume must be between 0 and 10.");
	}
	float volume = (float)i_volume / 10.0f;
	if (i_forAll) {
		for (int i = 0; i < m_soundController->GetNumChannels(); i++) {
			m_soundController->SetVolume(volume, i);
		}
	}
	else {
		m_soundController->SetVolume(volume, m_currentChannel);
	}
}

SoundClient::~SoundClient()
{
	if (m_soundController != nullptr) {
		delete m_soundController;
		m_soundController = nullptr;
	}
}
